# Intro
Portavita TMXMerger is based on TMXMerger 1.1 by Henry Pijffers.
(https://omegat.org/files/TMXMerger.zip)

It merges TMX files (even of different languages) into a single file.

Added since 1.1 is the option to create from a TMX with language `A->B` and `A->C` a new TMX with language `B->C`

TUVs for A or TUs without B and C are removed, so you end up with a clean `B->C` TMX

# Compile
`mvn package`

this will produce a jar file in ./target/

# Run
`java -jar target/TMXMerger-1.2.jar [source=LL(-CC)] [newsource=LL(-CC)] [newtarget=LL(-CC)] <tmx1> <tmx2> [<tmx3> ...] <output>`

If source is not given, it will set source language equal to the first TMX.

If newsource is given, the target TMX will have that language as source.

If newtarget is given, then the TMX is cleaned and will only contain TU's with a valid TU for source and target language.
Other TU\[V\]s are removed.
