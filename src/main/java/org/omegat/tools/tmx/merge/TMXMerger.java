/*
 * TMXMerger - Merges two or more TMX files
 * Copyright(c) 2005-2010, Henry Pijffers (henry.pijffers@saxnot.com)
 *
 * This program is licensed to you under the terms of version 2 or later of
 * the GNU General Public License (the "GPL"), as published by the Free Software
 * Foundation.
 */

package org.omegat.tools.tmx.merge;

/**
 * Merges two or more TMX files
 *
 * @author Henry Pijffers (henry.pijffers@saxnot.com)
 */
public class TMXMerger {

    public static void main(String[] arguments) {
        // FIX: check arguments
        
        // check number of arguments
        if (arguments.length < 3) {
            // print usage info
            System.err.println("Usage: Portavita TMXMerger [source=LL(-CC)] [newsource=LL(-CC)] [newtarget=LL(-CC)] <tmx1> <tmx2> [<tmx3> ...] <output>");
            System.err.println("If no source language is specified, the source language as");
            System.err.println("specified in the source language attribute in the first TMX");
            System.err.println("file will be used. To specify a source language, add the");
            System.err.println("option source=LL(-CC), where LL is the language code, and");
            System.err.println("CC is the country code (optional). This *must* be the first");
            System.err.println("argument.");
            
            // halt program
            return;
        }
        int argumentIndex=0;
        // get the source language (first argument)
        String sourceLanguage = arguments[argumentIndex].startsWith("source=")
                                    ? arguments[argumentIndex].substring(7)
                                    : null;
                                    
        // if the source language is not specified, notify the user
        if (sourceLanguage == null) {
            System.err.println("Source language not specified, using source language from first TMX file.");
        } else {
            argumentIndex++;
        }

        String newSourceLanguage = arguments[argumentIndex].startsWith("newsource=")
                ? arguments[argumentIndex].substring(10)
                : null;

        // if the source language is not specified, notify the user
        if (newSourceLanguage == null) {
            System.err.println("New source language not specified. Source lang will not change in output");
        } else {
            argumentIndex++;
        }

        String newTargetLanguage = arguments[argumentIndex].startsWith("newtarget=")
                ? arguments[argumentIndex].substring(10)
                : null;

        // if the source language is not specified, notify the user
        if (newTargetLanguage == null) {
            System.err.println("New target language not specified. All languages will remain in output");
        } else {
            argumentIndex++;
        }
        
        // determine the first file argument
        int firstFile = argumentIndex;
        
        try {
            // load the first TMX file
            TMX tmx = new TMX(sourceLanguage);
            tmx.load(arguments[firstFile]);
            
            // merge in all other TMX files
            for (int i = firstFile + 1; i < (arguments.length - 1); i++)
                tmx.merge(arguments[i]);
            
            // save the merged TMX to the file specified in the last argument
            // FIX: check if the file already exists, and ask for permission to overwrite
            tmx.save(arguments[arguments.length - 1], newSourceLanguage, newTargetLanguage);
        }
        catch (java.io.IOException exception) {
            System.err.println(exception.getLocalizedMessage());
            exception.printStackTrace(System.err);
        }
    }

}
